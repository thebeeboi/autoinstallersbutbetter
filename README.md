
# Discord-Linux Auto-Installers

These scripts reside on the ssh.surf domain for our users to automatically configure software.

There  is a built in client that lives within each newly generated container.

The script is located at: /usr/bin/auto-install

The script reads from /var/OS and uses the information there to match to a directory on the webserver, like so:
https://ssh.surf/ubuntu/wp



Any script not listed in a "list" file is experimental and NOT meant for production use yet.


Want Changes? - Feel free to submit a pull request OR open an issue within this repo.

